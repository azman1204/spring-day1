package com.example.spring_tot.models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "film")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Film {
    @Id
    @Column(name = "film_id")
    private Long filmId;

    @Column
    private String title;

    @Column
    private String description;

    @ManyToMany(mappedBy = "actorFilm")
    private Set<Actor> filmActor = new HashSet<>();
}
