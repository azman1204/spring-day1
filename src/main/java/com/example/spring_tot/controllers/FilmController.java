package com.example.spring_tot.controllers;

import com.example.spring_tot.models.Film;
import com.example.spring_tot.services.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/film")
public class FilmController {
    @Autowired
    private FilmService filmService;

    @GetMapping("/all")
    public List<Film> getAll() {
        return filmService.getAll();
    }

    @GetMapping("/one")
    public Optional<Film> getOne(@RequestParam Long filmId) {
        return filmService.getOne(filmId);
    }

    @GetMapping("/search")
    public Page search(@RequestParam String title) {
        return filmService.searchByTitle(title);
    }
}
