package com.example.spring_tot.controllers;

import com.example.spring_tot.models.Employee;
import com.example.spring_tot.services.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    private EmployeeService empSvc;

    public EmployeeController(EmployeeService empSvc) {
        this.empSvc = empSvc;
    }


    // employee/list
    @GetMapping("list")
    public List listAll() {
        return this.empSvc.findAll();
    }

    //@RequestBody - map kan kan data yg di post ke Employee model
    @PostMapping("add")
    public Employee add(@RequestBody Employee employee) {
        return this.empSvc.addEmployee(employee);
    }

    @PostMapping("update")
    public Employee update(@RequestBody Employee employee) {
        return this.empSvc.updateEmployee(employee);
    }

    @GetMapping("delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        this.empSvc.deleteEmployee(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("find/{id}")
    public Optional<Employee> findOne(@PathVariable("id") Long id) {
        return this.empSvc.findOne(id);
    }
}
