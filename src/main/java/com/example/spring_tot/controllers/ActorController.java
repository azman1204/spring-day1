package com.example.spring_tot.controllers;

import com.example.spring_tot.models.Actor;
import com.example.spring_tot.services.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/actor")
public class ActorController {
    @Autowired
    private ActorService actorService;

    @GetMapping("/all")
    public List<Actor> getAll() {
        return actorService.getAll();
    }

    @GetMapping("/one")
    public Optional<Actor> getOne(@RequestParam Long actorId) {
        return actorService.getOne(actorId);
    }
}
