package com.example.spring_tot.services;

import com.example.spring_tot.models.Film;
import com.example.spring_tot.repos.FilmRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FilmService {
    @Autowired
    private FilmRepo filmRepo;

    public List<Film> getAll() {
        return filmRepo.findAll();
    }

    public Optional<Film> getOne(Long filmId) {
        return filmRepo.findById(filmId);
    }

    public Page searchByTitle(String title) {
        Pageable pageable = PageRequest.of(0, 5, Sort.by("title"));
        return filmRepo.findAllWithoutActor(title, pageable);
        //return filmRepo.findByTitleContaining(title);
    }
}
