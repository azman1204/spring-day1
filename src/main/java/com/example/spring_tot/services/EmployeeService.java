package com.example.spring_tot.services;

import com.example.spring_tot.models.Employee;
import com.example.spring_tot.repos.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public List<Employee> findAll() {
        return this.employeeRepo.findAll();
    }

    public Employee addEmployee(Employee employee) {
        return this.employeeRepo.save(employee);
    }

    public Employee updateEmployee(Employee employee) {
        return this.employeeRepo.save(employee);
    }

    public void deleteEmployee(Long id) {
        this.employeeRepo.deleteById(id);
    }

    public Optional<Employee> findOne(Long id) {
        return this.employeeRepo.findById(id);
    }
}
