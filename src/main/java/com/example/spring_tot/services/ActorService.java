package com.example.spring_tot.services;

import com.example.spring_tot.models.Actor;
import com.example.spring_tot.repos.ActorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ActorService {
    @Autowired
    private ActorRepo actorRepo;

    public List<Actor> getAll() {
        return actorRepo.findAll();
    }

    public Optional<Actor> getOne(Long actorId) {
        return actorRepo.findById(actorId);
    }

}
