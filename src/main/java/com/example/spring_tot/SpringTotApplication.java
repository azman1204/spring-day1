package com.example.spring_tot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
public class SpringTotApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTotApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}

	@GetMapping("/student")
	public Student getStudent() {
		return new Student("John", "Doe");
	}

	@GetMapping("/student/list")
	public ArrayList getStudents() {
		ArrayList<Student> students = new ArrayList<>();
		students.add(new Student("John", "Doe"));
		students.add(new Student("Abu", "Hassan"));
		students.add(new Student("Azman", "Zakaria"));
		return students;
	}
}
