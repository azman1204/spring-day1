package com.example.spring_tot.repos;

import com.example.spring_tot.models.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FilmRepo extends JpaRepository<Film, Long> {
    public List<Film> findByTitleContaining(String title);

    // JPQL
    @Query(value = "SELECT film_id, title, description FROM Film WHERE title LIKE CONCAT('%',:title,'%')", nativeQuery = true)
    public List<Film> findTitle(@Param("title") String title);

    @Query("SELECT a.title, a.description FROM Film a WHERE title LIKE CONCAT('%',:title,'%')")
    public Page findAllWithoutActor(@Param("title") String title, Pageable pageable);
}
