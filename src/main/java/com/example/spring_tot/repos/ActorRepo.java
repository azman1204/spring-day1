package com.example.spring_tot.repos;

import com.example.spring_tot.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorRepo extends JpaRepository<Actor, Long> {

}
